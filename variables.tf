# ABC multi-use variables (env variable-defined)
variable "abc_security_group_id" {
    description = "Security group for all ABC servers"
}

variable "abc_team_name" {
    description = "Team name"
}

variable "abc_subnet_id" {
    description = "ENV subnet ID"
}

variable "abc_key_pair_name" {
    description = "us-east-1 ssh keypair"
}

# ABC web server variables (module-defined)
variable "abc_web_count" {
    description = "Number of web servers to provision"
    default = 2
}

variable "abc_web_ami_id" {
    description = "Region-specific AMI to be deployed"
    default = "ami-0ff8a91507f77f867"
}

variable "abc_web_instance_type" {
    description = "EC2 instance type"
    default = "t2.micro"
}

variable "abc_web_tag_app_name" {
    description = "Server naming prefix"
    default = "ABCWEB"
}

# ABC load balancer server variables (module-defined)
variable "abc_lb_count" {
    description = "Number of lb servers to provision"
    default = 2
}

variable "abc_lb_ami_id" {
    description = "Region-specific AMI to be deployed"
    default = "ami-0ff8a91507f77f867"
}

variable "abc_lb_instance_type" {
    description = "EC2 instance type"
    default = "t2.micro"
}

variable "abc_lb_tag_app_name" {
    description = "Server naming prefix"
    default = "ABCLB"
}