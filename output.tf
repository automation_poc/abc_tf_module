output "abc_web_ip_addrs" {
    value = "${module.abc_web_servers.ec2_ip_addrs}"
}

output "abc_lb_ip_addrs" {
    value = "${module.abc_lb_servers.ec2_ip_addrs}"
}