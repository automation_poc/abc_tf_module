# ABC web servers
module "abc_web_servers" {
    source = ""

    ec2_count = "${var.abc_web_count}"
    ec2_ami =  "${var.abc_web_ami_id}"
    ec2_instance_type = "${var.abc_web_instance_type}"
    ec2_key_pair_name = "${var.abc_key_pair_name}"
    ec2_security_group_id = "${var.abc_security_group_id}"
    ec2_subnet_id = "${var.abc_subnet_id}"

    ec2_tag_app_name = "${var.abc_web_tag_app_name}"
    ec2_team_name = "${var.abc_team_name}"
}

# ABC load balancer server
module "abc_lb_servers" {
    source = ""

    ec2_count = "${var.abc_lb_count}"
    ec2_ami =  "${var.abc_lb_ami_id}"
    ec2_instance_type = "${var.abc_lb_instance_type}"
    ec2_key_pair_name = "${var.abc_key_pair_name}"
    ec2_security_group_id = "${var.abc_security_group_id}"
    ec2_subnet_id = "${var.abc_lb_subnet_id}"

    ec2_tag_app_name = "${var.abc_lb_tag_app_name}"
    ec2_team_name = "${var.abc_team_name}"
}