########## ABC ##########
module "app_abc" {
    source = "https://gitlab.com/automation_poc/abc_tf_module.git"

    abc_team_name = "${var.env_team_name}"
    abc_security_group_id = "${var.env_sg_id}"
    abc_subnet_id = "${var.env_subnet_id}"
    abc_key_pair_name = "${var.env_key_pair}"
}

output "abc_web_ip_addrs" {
    value = "${module.app_abc.abc_web_ip_addrs}"
}
output "abc_lb_ip_addrs" {
    value = "${module.app_abc.abc_lb_ip_addrs}"
}
########## ABC ##########